-- answer a.
  SELECT * FROM artists WHERE name LIKE "%d%";

-- answer b.
  SELECT * FROM songs WHERE length < 230;

-- answer c.
  SELECT songs.song_name, songs.length, albums.album_title FROM songs JOIN albums ON albums.id = songs.album_id;

-- answer d.
  SELECT * FROM albums JOIN artists ON artists.id = albums.artist_id WHERE album_title LIKE "%a%";

-- answer e.
  SELECT * FROM albums ORDER BY album_title DESC LIMIT 0, 4;

-- answer f.
  SELECT * FROM albums JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC;
